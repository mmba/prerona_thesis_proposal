df = pd.read_csv(schedule_file_name, parse_dates=['start', 'end'])
dt = datetime.now()
df = df[df.end > dt]
df = df[df.start - timedelta(minutes=preheat_time) <= dt]
count = df.count()[0]
is_event_scheduled = count >= 1